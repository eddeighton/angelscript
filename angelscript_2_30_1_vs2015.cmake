
##assuming THIRD_PARTY_DIR

######################################
#Sort out Angel Script Dependencies
#add_definitions(-DANGELSCRIPT_DLL_LIBRARY_IMPORT)
set( ANGELSCRIPT_ADDON NAMES autowrapper PATHS ${THIRD_PARTY_DIR}/angelscript/sdk/add_on )
find_path( ANGELSCRIPT_INCLUDE NAMES REQUIRED angelscript.h PATHS ${THIRD_PARTY_DIR}/angelscript/sdk/angelscript/include )
set( ANGELSCRIPT_LIB_DIR ${THIRD_PARTY_DIR}/angelscript/sdk/angelscript/lib )

find_library( ANGELSCRIPT_LIB_RELEASE REQUIRED names angelscript.lib PATHS ${ANGELSCRIPT_LIB_DIR}/Release )
find_library( ANGELSCRIPT_LIB_DEBUG REQUIRED names angelscriptd.lib PATHS ${ANGELSCRIPT_LIB_DIR}/Debug )

file( GLOB ANGELSCRIPT_ADDONS_SRC_HEADERS 
			${ANGELSCRIPT_ADDON}/scripthelper/*.h 
			${ANGELSCRIPT_ADDON}/scriptbuilder/*.h 
			${ANGELSCRIPT_ADDON}/scriptstdstring/*.h 
			${ANGELSCRIPT_ADDON}/contextmgr/*.h 
			${ANGELSCRIPT_ADDON}/scriptarray/*.h )
file( GLOB ANGELSCRIPT_ADDONS_SRC_CPP 
			${ANGELSCRIPT_ADDON}/scripthelper/*.cpp 
			${ANGELSCRIPT_ADDON}/scriptbuilder/*.cpp
			${ANGELSCRIPT_ADDON}/scriptstdstring/*.cpp 
			${ANGELSCRIPT_ADDON}/contextmgr/*.cpp 
			${ANGELSCRIPT_ADDON}/scriptarray/*.cpp )

set( ANGEL_SCRIPT_ADDONS_SRC ${ANGELSCRIPT_ADDONS_SRC_HEADERS} ${ANGELSCRIPT_ADDONS_SRC_CPP} )
#message( "Found angel script addons: " ${ANGEL_SCRIPT_ADDONS_SRC} )

include_directories( ${ANGELSCRIPT_INCLUDE} ${ANGELSCRIPT_ADDON} )

#link_directories( optimized ${ANGELSCRIPT_LIB_DIR}/Release debug ${ANGELSCRIPT_LIB_DIR}/Debug )

source_group( angelscript FILES ${ANGEL_SCRIPT_ADDONS_SRC} )

install( FILES ${ANGELSCRIPT_LIB_DIR}/Debug/angelscriptd.dll DESTINATION bin)
install( FILES ${ANGELSCRIPT_LIB_DIR}/Release/angelscript.dll DESTINATION bin)

function( link_angelscript targetname )
	target_link_libraries( ${targetname} optimized ${ANGELSCRIPT_LIB_RELEASE} debug ${ANGELSCRIPT_LIB_DEBUG} )
endfunction( link_angelscript )
